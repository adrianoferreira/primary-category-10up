<?php

class PC10UP_Custom_Meta_Box {

	const NONCE_ACTION = 'primary-category-10up';
	const FIELD_KEY    = 'primary_category_10up';
	const NONCE_KEY    = 'primary_category_10up_nonce';

	public function add_hooks() {
		add_action( 'add_meta_boxes', array( $this, 'add_meta_box' ) );
		add_action( 'save_post', array( $this, 'save_post_data' ) );
	}

	public function add_meta_box() {
		foreach ( get_post_types() as $post_type ) {
			if ( 'page' === $post_type ) {
				continue;
			}

			add_meta_box( 'primary_category_10up', __( 'Primary Category', 'primary-category-10up' ), array( $this, 'render_metabox' ), $post_type, 'side' );
		}
	}

	public function render_metabox( $post ) {
		wp_nonce_field( self::NONCE_ACTION, self::NONCE_KEY );

		$value = get_post_meta( $post->ID, self::FIELD_KEY, true );

		$content = '<label for="myplugin_new_field">';
		$content .= __( 'Category ID', 'primary-category-10up' );
		$content .= '</label>';
		$content .= '<input type="text" id="' . self::FIELD_KEY . '-id" name="' . self::FIELD_KEY . '" value="' . esc_attr( $value ) . '" size="25" />';

		echo $content;
	}

	/**
	 * @param int $post_id
	 */
	public function save_post_data( $post_id ) {
		$category_id = isset( $_POST[ self::FIELD_KEY ] ) ? (int) $_POST[ self::FIELD_KEY ] : null;

		if ( ! $category_id || ! $this->is_valid_request() ) {
			return;
		}

		update_post_meta( $post_id, self::FIELD_KEY, $category_id );
	}

	/**
	 * @return bool
	 */
	private function is_valid_request() {
		return isset( $_POST[ self::NONCE_KEY ] ) && wp_verify_nonce( $_POST[ self::NONCE_KEY ], self::NONCE_ACTION );
	}
}