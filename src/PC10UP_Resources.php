<?php

class PC10UP_Resources {

	public function add_hooks() {
		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_resources' ) );
	}

	/**
	 * @param string $hook
	 */
	public function enqueue_resources( $hook ) {
		if ( in_array( $hook, array( 'post-new.php', 'post.php' ), true ) ) {
			wp_enqueue_script( 'pc10up-autocomplete-categories', PC10UP_PLUGIN_URL . '/res/js/categories-autocomplete.js', array( 'jquery-ui-autocomplete', 'jquery' ), false, true );
			wp_localize_script( 'pc10up-autocomplete-categories', 'categoriesList', $this->get_categories() );
		}
	}

	/**
	 * @return array
	 */
	private function get_categories() {
		$categories = array();

		foreach ( get_categories( array( 'hide_empty' => false ) ) as $category ) {
			$categories[] = array(
				'label' => $category->name,
				'id'    => $category->term_id,
			);
		}

		return $categories;
	}
}