jQuery(document).ready(function() {
	jQuery("#primary_category_10up-id")
		.autocomplete({
										source:    categoriesList,
										select:    function (event, ui) {
											jQuery("#primary_category_10up-id").val(ui.item.id);
											return false;
										}
									});
});