<?php

/*
	Plugin Name: Primary Category 10up
	Description: Lets users select a primary category for posts and custom post types
	Author: Adriano Ferreira
	Version: 1.0.0
*/

$primary_category_10up_url = untrailingslashit( plugin_dir_url( __FILE__ ) );

define( 'PC10UP_PLUGIN_PATH', __DIR__ );
define( 'PC10UP_PLUGIN_URL', $primary_category_10up_url );

require_once PC10UP_PLUGIN_PATH . '/vendor/autoload.php';

$pc10up_meta_box = new PC10UP_Custom_Meta_Box();
$pc10up_meta_box->add_hooks();

$pc10up_resources = new PC10UP_Resources();
$pc10up_resources->add_hooks();