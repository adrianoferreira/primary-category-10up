<?php

/**
 * Class Test_PC10UP_Resources
 * @group primary-category
 */
class Test_PC10UP_Resources extends PHPUnit_Framework_TestCase {

	public function setUp() {
		WP_Mock::setUp();
	}

	/**
	 * @test
	 */
	public function it_adds_hooks() {
		$subject = new PC10UP_Resources();

		WP_Mock::expectActionAdded( 'admin_enqueue_scripts', array( $subject, 'enqueue_resources' ) );

		$subject->add_hooks();
	}

	/**
	 * @test
	 * @dataProvider dp_pages
	 */
	public function it_enqueue_scripts( $page ) {
		$subject = new PC10UP_Resources();

		WP_Mock::userFunction( 'wp_enqueue_script', array(
			'args'  => array(
				'pc10up-autocomplete-categories',
				PC10UP_PLUGIN_URL . '/res/js/categories-autocomplete.js',
				array( 'jquery-ui-autocomplete', 'jquery' ),
				false,
				true
			),
			'times' => 1,
		) );

		$category          = new stdClass();
		$category->term_id = 1;
		$category->name    = 'test';

		$categories = array( $category );

		WP_Mock::userFunction( 'wp_localize_script', array(
			'args'  => array(
				'pc10up-autocomplete-categories',
				'categoriesList',
				array(
					array( 'label' => 'test', 'id' => 1 ),
				)
			),
			'times' => 1,
		) );

		WP_Mock::userFunction( 'get_categories', array(
			'return' => $categories,
			'args' => array( array( 'hide_empty' => false ) ),
		) );

		$subject->enqueue_resources( $page );
	}

	public function dp_pages() {
		return array(
			array( 'post.php' ),
			array( 'post-new.php' ),
		);
	}

	/**
	 * @test
	 */
	public function it_does_not_enqueue_scripts_when_screen_is_invalid() {
		$subject = new PC10UP_Resources();

		WP_Mock::userFunction( 'wp_enqueue_script', array(
			'times' => 0,
		) );

		WP_Mock::userFunction( 'wp_localize_script', array(
			'times' => 0,
		) );

		$subject->enqueue_resources( 'some-other-page.php' );
	}
}