<?php
/**
 * Class Test_PC10UP_Custom_Meta_Box
 *
 * @group primary-category
 */

class Test_PC10UP_Custom_Meta_Box extends PHPUnit_Framework_TestCase {

	public function setUp() {
		WP_Mock::setUp();
	}

	/**
	 * @test
	 */
	public function it_adds_hooks() {
		$subject = new PC10UP_Custom_Meta_Box();

		WP_Mock::expectActionAdded( 'add_meta_boxes', array( $subject, 'add_meta_box' ) );
		WP_Mock::expectActionAdded( 'save_post', array( $subject, 'save_post_data' ) );

		$subject->add_hooks();
	}

	/**
	 * @test
	 */
	public function it_adds_meta_boxes() {
		$subject = new PC10UP_Custom_Meta_Box();

		WP_Mock::userFunction( 'get_post_types', array(
			'return' => array( 'post', 'custom-post-type', 'page' ),
		) );

		WP_Mock::userFunction( 'add_meta_box', array(
			'times' => 1,
			'args' => array( 'primary_category_10up', 'Primary Category', array( $subject, 'render_metabox' ), 'post', 'side' ),
		) );

		WP_Mock::userFunction( 'add_meta_box', array(
			'times' => 1,
			'args' => array( 'primary_category_10up', 'Primary Category', array( $subject, 'render_metabox' ), 'custom-post-type', 'side' ),
		) );

		$subject->add_meta_box();
	}

	/**
	 * @test
	 */
	public function it_renders_metabox() {
		$subject = new PC10UP_Custom_Meta_Box();

		$category = 1;

		$post = new stdClass();
		$post->ID = 2;

		WP_Mock::userFunction( 'get_post_meta', array(
			'args' => array( $post->ID, PC10UP_Custom_Meta_Box::FIELD_KEY, true ),
			'return' => $category,
		) );

		WP_Mock::userFunction( 'wp_nonce_field', array() );

		$content = '<label for="myplugin_new_field">';
		$content .= __( 'Category ID', 'primary-category-10up' );
		$content .= '</label>';
		$content .= '<input type="text" id="' . PC10UP_Custom_Meta_Box::FIELD_KEY . '-id" name="' . PC10UP_Custom_Meta_Box::FIELD_KEY . '" value="' . $category . '" size="25" />';

		ob_start();
		$subject->render_metabox( $post );
		$rendered_content = ob_get_contents();
		ob_end_clean();
		$this->assertEquals( $content, $rendered_content );
	}

	/**
	 * @test
	 */
	public function it_saves_metabox() {
		$subject = new PC10UP_Custom_Meta_Box();

		$_POST[ PC10UP_Custom_Meta_Box::FIELD_KEY ] = 2;
		$_POST[ PC10UP_Custom_Meta_Box::NONCE_KEY ] = 'nonce';
		$post_id = 1;

		WP_Mock::userFunction( 'wp_verify_nonce', array(
			'return' => true,
			'args' => array( $_POST[ PC10UP_Custom_Meta_Box::NONCE_KEY ], PC10UP_Custom_Meta_Box::NONCE_ACTION )
		) );

		WP_Mock::userFunction( 'update_post_meta', array(
			'args' => array( $post_id, PC10UP_Custom_Meta_Box::FIELD_KEY, $_POST[ PC10UP_Custom_Meta_Box::FIELD_KEY ] ),
			'times' => 1,
		) );

		$subject->save_post_data( $post_id );
	}

	/**
	 * @test
	 */
	public function it_does_not_save_if_category_is_not_valid() {
		$subject = new PC10UP_Custom_Meta_Box();

		$_POST[ PC10UP_Custom_Meta_Box::FIELD_KEY ] = 2;
		$_POST[ PC10UP_Custom_Meta_Box::NONCE_KEY ] = 'nonce';
		$post_id = 1;

		WP_Mock::userFunction( 'wp_verify_nonce', array(
			'return' => false,
			'args' => array( $_POST[ PC10UP_Custom_Meta_Box::NONCE_KEY ], PC10UP_Custom_Meta_Box::NONCE_ACTION )
		) );

		WP_Mock::userFunction( 'update_post_meta', array(
			'times' => 0,
		) );

		$subject->save_post_data( $post_id );
	}

	/**
	 * @test
	 */
	public function it_does_not_save_if_category_is_not_set() {
		$subject = new PC10UP_Custom_Meta_Box();

		unset( $_POST[ PC10UP_Custom_Meta_Box::FIELD_KEY ] );
		$_POST[ PC10UP_Custom_Meta_Box::NONCE_KEY ] = 'nonce';
		$post_id = 1;

		WP_Mock::userFunction( 'wp_verify_nonce', array(
			'return' => false,
			'args' => array( $_POST[ PC10UP_Custom_Meta_Box::NONCE_KEY ], PC10UP_Custom_Meta_Box::NONCE_ACTION )
		) );

		WP_Mock::userFunction( 'update_post_meta', array(
			'times' => 0,
		) );

		$subject->save_post_data( $post_id );
	}

	/**
	 * @test
	 */
	public function it_does_not_save_if_request_is_not_valid() {
		$subject = new PC10UP_Custom_Meta_Box();

		$_POST[ PC10UP_Custom_Meta_Box::FIELD_KEY ] = 'aaaaa';
		$_POST[ PC10UP_Custom_Meta_Box::NONCE_KEY ] = 'nonce';
		$post_id = 1;

		WP_Mock::userFunction( 'wp_verify_nonce', array(
			'return' => true,
			'args' => array( $_POST[ PC10UP_Custom_Meta_Box::NONCE_KEY ], PC10UP_Custom_Meta_Box::NONCE_ACTION )
		) );

		WP_Mock::userFunction( 'update_post_meta', array(
			'times' => 0,
		) );

		$subject->save_post_data( $post_id );
	}
}